#ifndef COMMAND_BUFFER_T_H
#define COMMAND_BUFFER_T_H

#include <cstring>
#include <span>
#include <string>
#include <string_view>
#include <type_traits>

#include "c_api.h"

struct SM_IPC_EXPORT command_buffer_t {
    command_buffer_t();

    std::span<uint8_t, raw_command_buffer_t::buffer_size_k> get();

    off_t read_offset();
    off_t write_offset();

    void read_offset( off_t offset );
    void write_offset( off_t offset );

    void reset_read_offset();
    void reset_write_offset();
    void reset_offsets();

    template<typename T> T read() {
        if constexpr ( std::is_same_v<T, std::string_view> || std::is_same_v<T, std::string> || std::is_same_v<T, const char *> ) {
            auto value = std::string_view( (char *)&get()[read_offset()] );
            read_offset( read_offset() + value.length() + 1 );
            if constexpr ( std::is_same_v<T, std::string> || std::is_same_v<T, const char *> )
                return value.data();
            else
                return value;
        } else {
            auto value = *reinterpret_cast<T *>( &get()[read_offset()] );
            read_offset( read_offset() + sizeof( T ) );
            return value;
        }
    }

    template<typename T> void write( T value ) {
        if constexpr ( std::is_same_v<T, std::string_view> || std::is_same_v<T, std::string> ) {
            std::memcpy( &get()[write_offset()], value.data(), value.length() );
            ( &get()[write_offset()] )[value.length()] = '\0';
            write_offset( write_offset() + value.length() + 1 );
        } else if constexpr ( std::is_same_v<T, const char *> ) {
            std::strcpy( &get()[write_offset()], value );
            write_offset( write_offset() + value.length() + 1 );
        } else {
            *reinterpret_cast<T *>( &get()[write_offset()] ) = value;
            write_offset( write_offset() + sizeof( T ) );
        }
    }

	template <typename T, typename ... Args> void write( T value, Args ... values ) {
		write( value );
		write( values... );
	}

private:
    raw_command_buffer_t data_;
};

#endif // COMMAND_BUFFER_T_H
