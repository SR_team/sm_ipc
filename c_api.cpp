#include "c_api.h"

#include "Ipc.h"

int ipc_get_host_pid() {
    return Ipc::get().get_host_pid();
}

void *ipc_alloc( const char *name, off_t size, int target_pid ) {
    return Ipc::get().alloc( name, size, target_pid );
}

int ipc_free( const char *name, int target_pid ) {
    return Ipc::get().free( name, target_pid );
}

ipc_ptr_t ipc_find( const char *name, int target_pid ) {
    return Ipc::get().find( name, target_pid );
}

void ipc_process() {
    return Ipc::get().process();
}

int ipc_register_cmd(int id, int (*callback)(raw_command_buffer_t *) ) {
    return Ipc::get().registr_cmd( id, [callback]( command_buffer_t &buffer ) {
        return callback( reinterpret_cast<raw_command_buffer_t *>( &buffer ) );
    } );
}

int ipc_unregistr_cmd( int id ) {
    return Ipc::get().unregistr_cmd( id );
}

void ipc_call_cmd( int id, void ( *callback )( raw_command_buffer_t *, action_cmd ), int pid, unsigned long long wait_ns ) {
    Ipc::get().call_cmd(
        id,
        [callback]( command_buffer_t &buffer, action_cmd action ) {
            if ( callback ) callback( reinterpret_cast<raw_command_buffer_t *>( &buffer ), action );
        },
        pid,
        std::chrono::nanoseconds{ wait_ns } );
}
