#include "host_info_t.h"

host_info_t::host_info_t( int host_pid ) : process_id_( host_pid ) {}

int host_info_t::host_pid() const {
    return process_id_;
}

command_t &host_info_t::command() {
    return command_;
}
