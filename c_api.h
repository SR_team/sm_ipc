#ifndef C_API_H
#define C_API_H

#include "sm_ipc_global.h"

#include <unistd.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifndef __cplusplus
enum { command_buffer_size_k = 4096 };
#endif
struct raw_command_buffer_t {
#ifdef __cplusplus
    enum { buffer_size_k = 4096 };

    uint8_t data[buffer_size_k];
#else
    uint8_t data[command_buffer_size_k];
#endif
    off_t read_offset;
    off_t write_offset;
};

struct ipc_ptr_t {
    void *ptr;
    off_t size;
};

enum { host_target_pid = -1 };

#ifdef __cplusplus
enum class action_cmd {
    read_failure,
    read_success,
    write
};
#else
enum action_cmd {
    action_cmd_read_failure,
    action_cmd_read_success,
    action_cmd_write
};

enum { wait_default = 30000000000 };
#endif

SM_IPC_EXPORT int ipc_get_host_pid();

SM_IPC_EXPORT void *ipc_alloc( const char *name, off_t size, int target_pid );
SM_IPC_EXPORT int ipc_free( const char *name, int target_pid );
SM_IPC_EXPORT struct ipc_ptr_t ipc_find( const char *name, int target_pid );

SM_IPC_EXPORT int ipc_register_cmd( int id, int ( *callback )( struct raw_command_buffer_t * ) );
SM_IPC_EXPORT int ipc_unregistr_cmd( int id );
SM_IPC_EXPORT void ipc_call_cmd( int id, void ( *callback )( struct raw_command_buffer_t *, enum action_cmd ), int pid, unsigned long long wait_ns );

SM_IPC_EXPORT void ipc_process();

#ifdef __cplusplus
}
#endif

#endif // C_API_H
