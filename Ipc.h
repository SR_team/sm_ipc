#ifndef IPC_H
#define IPC_H

#include "sm_ipc_global.h"

#include <chrono>
#include <functional>
#include <vector>
#include <map>
#include <optional>

#include "command_buffer_t.h"

struct host_info_t;

class SM_IPC_EXPORT Ipc {
    static constexpr auto segment_name_k = "shared_memory_ipc2";

    struct memory_t {
        int pid = 0;
        off_t size = 0;
        void *ptr = nullptr;
        std::string name;
    };

    std::vector<memory_t> memory_;
    std::map<int, std::function<bool( command_buffer_t & )>> commands_;

    Ipc();
    ~Ipc();

public:
    enum {
        command_allocate = -2,
        command_deallocate = -3,
        command_findmem = -4
    };

    static Ipc &get();

    int get_host_pid() const;

    void *alloc( std::string_view name, off_t size, int pid = host_target_pid );
    bool free( std::string_view name, int pid = host_target_pid );
    ipc_ptr_t find( std::string_view name, int pid = host_target_pid );

    bool registr_cmd( int id, const std::function<bool( command_buffer_t & )> &callback );
    bool unregistr_cmd( int id );
    void call_cmd( int id, const std::function<void( command_buffer_t &, action_cmd )> &callback, int pid = host_target_pid, std::chrono::nanoseconds wait = std::chrono::seconds{30} );
    void call_cmd( int id, int pid = host_target_pid, std::chrono::nanoseconds wait = std::chrono::seconds{30} );

	template <typename ... Args>
	bool call_cmd( int id, int pid, std::chrono::nanoseconds wait, Args ... args ) {
		bool ret;
		call_cmd( id, [&]( command_buffer_t &buffer, action_cmd action ) {
			if ( action != action_cmd::write ) {
				ret = action == action_cmd::read_success;
				return;
			}
			buffer.reset_write_offset();
			buffer.write( args... );
		}, pid, wait );
		return ret;
	}

	template <typename Res, typename ... Args>
	auto call_cmd( int id, int pid, std::chrono::nanoseconds wait, Args ... args ) -> std::optional<Res> {
		std::optional<Res> ret;
		call_cmd( id, [&]( command_buffer_t &buffer, action_cmd action ) {
			if ( action == action_cmd::write ) {
				buffer.reset_write_offset();
				buffer.write( args... );
			} else if ( action == action_cmd::read_success ) {
				buffer.reset_read_offset();
				ret = buffer.read<Res>();
				return;
			}
			ret = std::nullopt;
		}, pid, wait );
		return ret;
	}

    void process();

protected:
    host_info_t *get_host_info() const;

    void allocate_host_info();

    void process_alloc();
    void process_free();
    void process_find();
};

#endif // IPC_H
