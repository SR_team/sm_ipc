#ifndef COMMAND_T_H
#define COMMAND_T_H

#include <cstdint>
#include <string>
#include <string_view>
#include <span>
#include <type_traits>

#include <boost/interprocess/sync/interprocess_mutex.hpp>
#include <boost/interprocess/sync/interprocess_semaphore.hpp>

#include "command_buffer_t.h"
#include "c_api.h"

struct host_info_t;

struct command_t {
    enum { invalid_command_id = -1 };

    enum class status_t : std::uint8_t {
        terminate,
        success,
        failed,
        wait,
        process
    };

    boost::interprocess::interprocess_mutex mutex;
    boost::interprocess::interprocess_semaphore ready{ 0 };
    status_t status = status_t::terminate;
    int id = invalid_command_id;
    int target_pid = host_target_pid;
    command_buffer_t buffer;

    struct deleter {
        deleter( host_info_t *host );
        ~deleter();

    private:
        host_info_t *host;
    };
};

#endif // COMMAND_T_H
