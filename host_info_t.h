#ifndef HOST_INFO_T_H
#define HOST_INFO_T_H

#include "command_t.h"

struct command_t;

struct host_info_t {
    host_info_t( int host_pid );
    boost::interprocess::interprocess_mutex mutex;

    int host_pid() const;

    command_t &command();

private:
    int process_id_;
    command_t command_;
};

#endif // HOST_INFO_T_H
