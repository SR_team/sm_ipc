#include "Ipc.h"

#include <algorithm>
#include <stdexcept>
#include <iostream>
#include <filesystem>


#include <boost/process/environment.hpp>
#include <boost/config.hpp>

#ifdef BOOST_OS_WINDOWS

#   include <fstream>
#   include <vector>

#	include <windows.h>
#	include <shlobj.h>

EXTERN_C IMAGE_DOS_HEADER __ImageBase;

static std::filesystem::path current_module() {
    wchar_t module_name[MAX_PATH];
    GetModuleFileNameW( (HMODULE)&__ImageBase, module_name, MAX_PATH );
    return module_name;
}

static std::filesystem::path get_dir( int dir_id ) {
	wchar_t system_path[MAX_PATH];
	if ( SHGetFolderPathW( nullptr, dir_id, nullptr, SHGFP_TYPE_CURRENT, system_path ) == S_OK ) return system_path;
	return {};
}

static std::vector<std::filesystem::path> get_shm_paths() {
	static constexpr auto kMountPoints = "/proc/mounts";
	static constexpr auto kShmType = "tmpfs";

	if ( !std::filesystem::exists( kMountPoints ) )
		return {};

	std::ifstream mounts( kMountPoints );
	if ( !mounts.is_open() )
		return {};

	static const auto skip_space = []( std::string_view &str ) {
		while ( str.starts_with( ' ' ) )
			str = str.substr( 1 );
	};

	static const auto find_separator = []( std::string_view str ) {
		int pos = -1;
		while ( pos < static_cast<int>(str.length()) ) {
			++pos;
			if ( !std::isspace( str[pos] ) )
				continue;
			if ( pos > 0 && str[pos - 1] == '\\' )
				continue;
			break;
		}

		return pos == str.length() ? -1 : pos;
	};

	std::vector<std::filesystem::path> list;
	std::string mount_point;
	while ( std::getline( mounts, mount_point ) ) {
		if ( !mount_point.starts_with( kShmType ) )
			continue;

		auto mp = std::string_view( &mount_point[std::strlen( kShmType )] );
		skip_space( mp );
		auto pos = find_separator( mp );
		if ( pos == -1 )
			continue;

		auto path = mp.substr( 0, pos );
		if ( !std::filesystem::exists( path ) )
			continue;

		mp = mp.substr( pos );
		skip_space( mp );

		pos = find_separator( mp );
		if ( pos == -1 )
			continue;

		// Skip type duplication
		mp = mp.substr( pos );
		skip_space( mp );
		pos = find_separator( mp );
		if ( pos == -1 )
			continue;

		auto options = mp.substr( 0, pos );
		if ( !options.starts_with( "rw," ) && !options.ends_with( ",rw" ) && options.find( ",rw," ) == std::string::npos )
			continue;

		if ( path.find( "shm" ) != std::string::npos )
			list.insert( list.begin(), path );
		else
			list.emplace_back( path );
	}
	return list;
}

static std::string utf16_to_cpacp( std::wstring_view utf16 ) {
    if ( utf16.empty() ) return "";
    const int cpacp_len = ::WideCharToMultiByte( CP_ACP, 0, utf16.data(), utf16.length(), nullptr, 0, 0, 0 );
    if ( cpacp_len == 0 ) return "";
    std::string cpacp( cpacp_len, '\0' );
    ::WideCharToMultiByte( CP_ACP, 0, utf16.data(), utf16.length(), cpacp.data(), cpacp.size(), 0, 0 );
    return cpacp;
}

static auto default_shm_path() {
	return current_module().parent_path() / "shm" / current_module().stem();
}

static auto shm_path( const std::filesystem::path &path, std::string_view shm_dir_name = "shm" ) {
	if ( !std::filesystem::exists( path ) ) return std::filesystem::path();

	return path / shm_dir_name / current_module().parent_path().stem() / current_module().stem();
}

static auto wine_shm_path( const std::filesystem::path &path ) {
	if ( !std::filesystem::exists( path ) ) return std::filesystem::path();

	static auto wine_get_version = reinterpret_cast<const char *(*)()>( GetProcAddress( GetModuleHandleA( "ntdll" ), "wine_get_version" ) );

	static auto prefix_ptr = getenv( "WINEPREFIX" );
	static std::string prefix = std::string( prefix_ptr == nullptr || *prefix_ptr == '\0' ? "default" : prefix_ptr ) +
	                            ( wine_get_version != nullptr ? '-' + std::string( wine_get_version() ) : "" );

	return shm_path( path, "wine-shm" ) / prefix;
}

static bool try_make_path( const std::filesystem::path &path ) {
	if ( std::filesystem::exists( path ) ) return true;
	try {
		return std::filesystem::create_directories( path );
	} catch ( std::filesystem::filesystem_error &e ) {
		return false;
	}
}

#	define BOOST_INTERPROCESS_SHARED_DIR_FUNC
namespace boost::interprocess::ipcdetail {
    void get_shared_dir( std::wstring &shared_dir ) {
	    static std::filesystem::path sm_ipc;
	    if ( sm_ipc.empty() ) {
		    auto sm_ipc_path = getenv( "SM_IPC_PATH" ); // TODO: use env on linux
		    if ( sm_ipc_path != nullptr && *sm_ipc_path != '\0' )
			    sm_ipc = sm_ipc_path;
		    auto shm_paths = get_shm_paths();
		    while ( !shm_paths.empty() && ( sm_ipc.empty() || !try_make_path( sm_ipc ) ) ) {
			    sm_ipc = wine_shm_path( shm_paths.front() );
			    shm_paths.erase( shm_paths.begin() );
		    }
		    if ( sm_ipc.empty() || !try_make_path( sm_ipc ) )  sm_ipc = shm_path( get_dir( CSIDL_INTERNET_CACHE ) );
		    if ( sm_ipc.empty() || !try_make_path( sm_ipc ) ) sm_ipc = default_shm_path();
	    }
	    if ( try_make_path( sm_ipc ) ) shared_dir = sm_ipc;
    }

    void get_shared_dir( std::string &shared_dir ) {
	    static std::string dir;
	    if ( dir.empty() ) {
		    std::wstring shared_dir_utf16;
		    get_shared_dir( shared_dir_utf16 );
		    dir = utf16_to_cpacp( shared_dir_utf16.c_str() );
	    }
	    shared_dir = dir;
    }
} // namespace boost::interprocess::ipcdetail

static bool is_process_present( int id ) {
    auto handle = OpenProcess( PROCESS_QUERY_LIMITED_INFORMATION, FALSE, id );
    if ( !handle ) return false;

    DWORD exit_code = 0;
    if ( !GetExitCodeProcess( handle, &exit_code ) ) {
        CloseHandle( handle );
        return false;
    }

    return exit_code == STILL_ACTIVE;
}
#else
// Enable native api - increase speed
#define BOOST_INTERPROCESS_FORCE_NATIVE_EMULATION

static bool is_process_present( int id ) {
    return std::filesystem::exists( "/proc/" + std::to_string( id ) );
}
#endif

#include <boost/interprocess/shared_memory_object.hpp>
#include <boost/interprocess/mapped_region.hpp>
#include <boost/interprocess/sync/scoped_lock.hpp>

#include "host_info_t.h"

using namespace std::chrono_literals;

namespace {
    boost::interprocess::shared_memory_object segment;
    boost::interprocess::mapped_region base_region;

	enum class remove_result {
		failed, success, forced, disown
	};

	remove_result force_remove_shared_memory( const char *name ) {
		auto removed = boost::interprocess::shared_memory_object::remove( name );

#ifdef BOOST_OS_WINDOWS
		static std::wstring shm_path;
		if ( shm_path.empty() )
			boost::interprocess::ipcdetail::get_shared_dir( shm_path );
		auto path = std::filesystem::path( shm_path ) / name;
		if ( removed && !std::filesystem::exists( path ) )
			return remove_result::success;

		try {
			if ( !std::filesystem::remove( path ) )
				return removed ? remove_result::disown : remove_result::failed;
			return removed ? remove_result::success : remove_result::forced;
		} catch ( std::exception &e ) {
			std::cerr << "Failed remove shm file for memory " << name << " - " << e.what() << std::endl;
		}

		return removed ? remove_result::disown : remove_result::failed;
#else
		return removed ? remove_result::success : remove_result::failed;
#endif
	}
} // namespace

Ipc::Ipc() {
    segment =
        boost::interprocess::shared_memory_object( boost::interprocess::open_or_create, segment_name_k, boost::interprocess::read_write );
    boost::interprocess::offset_t size = 0;
    if ( segment.get_size( size ) && size ) {
        base_region = boost::interprocess::mapped_region( segment, boost::interprocess::read_write );
        if ( get_host_pid() != boost::this_process::get_id() ) {
            if ( !is_process_present( get_host_pid() ) ) {
	            force_remove_shared_memory( segment_name_k );
                allocate_host_info();
            }
        }
    } else {
        allocate_host_info();
    }
}

Ipc::~Ipc() {
    for ( auto &&mem : memory_ ) { force_remove_shared_memory( mem.name.c_str() ); }

    if ( get_host_pid() == boost::this_process::get_id() ) force_remove_shared_memory( segment_name_k );
}

Ipc &Ipc::get() {
    static Ipc ipc;
    return ipc;
}

int Ipc::get_host_pid() const {
    return get_host_info()->host_pid();
}

void *Ipc::alloc( std::string_view name, off_t size, int pid ) {
    if ( name.empty() || !size ) return nullptr;

    void *result = nullptr;
    call_cmd(
        command_allocate,
        [&result, name, size]( command_buffer_t &buffer, action_cmd action ) {
            if ( action == action_cmd::write ) {
                buffer.write( name );
                buffer.write( size );
                buffer.write( boost::this_process::get_id() );
            } else if ( action == action_cmd::read_success ) {
                result = buffer.read<void *>();
            }
        },
        pid );

    return result;
}

bool Ipc::free( std::string_view name, int pid ) {
    if ( name.empty() ) return false;

    bool result = false;
    call_cmd(
        command_deallocate,
        [&result, name]( command_buffer_t &buffer, action_cmd action ) {
            if ( action == action_cmd::write ) {
                buffer.write( name );
                buffer.write( boost::this_process::get_id() );
            } else if ( action == action_cmd::read_success ) {
                result = true;
            }
        },
        pid );

    return result;
}

ipc_ptr_t Ipc::find( std::string_view name, int pid ) {
    if ( name.empty() ) return { nullptr, 0 };

    ipc_ptr_t result{ nullptr, 0 };
    call_cmd(
        command_findmem,
        [&result, name]( command_buffer_t &buffer, action_cmd action ) {
            if ( action == action_cmd::write ) {
                buffer.write( name );
            } else if ( action == action_cmd::read_success ) {
                result.ptr = buffer.read<void *>();
                result.size = buffer.read<off_t>();
            }
        },
        pid );
    return result;
}

bool Ipc::registr_cmd( int id, const std::function<bool( command_buffer_t & )> &callback ) {
    if ( id < 0 ) // builtin command space
        return false;
    if ( commands_.contains( id ) ) return false;

    commands_.insert( { id, callback } );
    return true;
}

bool Ipc::unregistr_cmd( int id ) {
    if ( !commands_.contains( id ) ) return false;

    commands_.erase( id );
    return true;
}

void Ipc::call_cmd( int id, const std::function<void( command_buffer_t &, action_cmd )> &callback, int pid, std::chrono::nanoseconds wait ) {
    boost::interprocess::scoped_lock lock( get_host_info()->command().mutex );

    get_host_info()->command().buffer.reset_write_offset();
    callback( get_host_info()->command().buffer, action_cmd::write );
    get_host_info()->command().id = id;
    get_host_info()->command().target_pid = ( pid == host_target_pid ? get_host_pid() : pid );

    command_t::deleter deleter( get_host_info() );

    if ( !get_host_info()->command().ready.timed_wait( std::chrono::system_clock::now() + wait ) )
        throw std::runtime_error( "Expire time of wait command" );

    get_host_info()->command().buffer.reset_read_offset();
    if ( get_host_info()->command().status == command_t::status_t::failed )
        callback( get_host_info()->command().buffer, action_cmd::read_failure );
    else
        callback( get_host_info()->command().buffer, action_cmd::read_success );
}

void Ipc::call_cmd( int id, int pid, std::chrono::nanoseconds wait ) {
    call_cmd(
        id,
        []( command_buffer_t &, action_cmd ) {},
        pid,
        wait );
}

void Ipc::process() {
    if ( get_host_info()->command().status != command_t::status_t::wait ) return;
    if ( get_host_info()->command().target_pid != boost::this_process::get_id() ) return;
    if ( get_host_info()->command().id == command_t::invalid_command_id ) return;

    boost::interprocess::scoped_lock lock( get_host_info()->mutex );
    get_host_info()->command().status = command_t::status_t::process;

    switch ( get_host_info()->command().id ) {
        case command_allocate:
            process_alloc();
            break;
        case command_deallocate:
            process_free();
            break;
        case command_findmem:
            process_find();
            break;
        default: {
            auto it = commands_.find( get_host_info()->command().id );
            if ( it == commands_.end() ) {
                std::cerr << "Unhandle command " << get_host_info()->command().id << std::endl;
                return;
            }
            get_host_info()->command().status =
                it->second( get_host_info()->command().buffer ) ? command_t::status_t::success : command_t::status_t::failed;
            break;
        }
    };

    get_host_info()->command().ready.post();
}

host_info_t *Ipc::get_host_info() const {
    return static_cast<host_info_t *>( base_region.get_address() );
}

void Ipc::allocate_host_info() {
    segment.truncate( sizeof( host_info_t ) );
    base_region = boost::interprocess::mapped_region( segment, boost::interprocess::read_write );

    new ( base_region.get_address() ) host_info_t( boost::this_process::get_id() );
}

void Ipc::process_alloc() {
    try {
        get_host_info()->command().buffer.reset_read_offset();
        auto name = get_host_info()->command().buffer.read<std::string_view>();
        auto size = get_host_info()->command().buffer.read<off_t>();
        auto pid = get_host_info()->command().buffer.read<int>();

        boost::interprocess::shared_memory_object shm;
        try {
            shm =
                boost::interprocess::shared_memory_object( boost::interprocess::create_only, name.data(), boost::interprocess::read_write );
        } catch ( ... ) {
            auto it = std::find_if( memory_.begin(), memory_.end(), [name]( const memory_t &mem ) { return mem.name == name; } );
            if ( it == memory_.end() ) {
                std::cerr << "Realloc alien memory " << name << std::endl;
	            force_remove_shared_memory( name.data() );
                shm = boost::interprocess::shared_memory_object( boost::interprocess::create_only,
                                                                 name.data(),
                                                                 boost::interprocess::read_write );
            }
        }
        shm.truncate( size );
        get_host_info()->command().status = command_t::status_t::success;
        boost::interprocess::mapped_region region( shm, boost::interprocess::read_write );
        memory_.push_back( memory_t{ pid, size, region.get_address(), std::string( name ) } );

        get_host_info()->command().buffer.reset_write_offset();
        get_host_info()->command().buffer.write( region.get_address() );
    } catch ( ... ) { get_host_info()->command().status = command_t::status_t::failed; }
}

void Ipc::process_free() {
    get_host_info()->command().buffer.reset_read_offset();
    auto name = get_host_info()->command().buffer.read<std::string_view>();
    auto pid = get_host_info()->command().buffer.read<int>();

    auto it =
        std::find_if( memory_.begin(), memory_.end(), [name, pid]( const memory_t &mem ) { return mem.pid == pid && mem.name == name; } );
    if ( it == memory_.end() )
        get_host_info()->command().status = command_t::status_t::failed;
    else {
        memory_.erase( it );
	    auto removed = force_remove_shared_memory( name.data() );
	    if ( removed == remove_result::success || removed == remove_result::forced ) {
		    get_host_info()->command().status = command_t::status_t::success;
	    } else {
		    get_host_info()->command().status = command_t::status_t::failed;
	    }
    }
}

void Ipc::process_find() {
    get_host_info()->command().buffer.reset_read_offset();
    auto name = get_host_info()->command().buffer.read<std::string_view>();
    auto it = std::find_if( memory_.begin(), memory_.end(), [name]( const memory_t &mem ) { return mem.name == name; } );

    if ( it == memory_.end() )
        get_host_info()->command().status = command_t::status_t::failed;
    else {
        get_host_info()->command().buffer.reset_write_offset();
        get_host_info()->command().buffer.write( it->ptr );
        get_host_info()->command().buffer.write( it->size );
        get_host_info()->command().status = command_t::status_t::success;
    }
}
