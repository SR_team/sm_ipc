#include "command_buffer_t.h"

command_buffer_t::command_buffer_t() {
    reset_offsets();
}

std::span<uint8_t, raw_command_buffer_t::buffer_size_k> command_buffer_t::get() {
    return data_.data;
}

off_t command_buffer_t::read_offset() {
    return data_.read_offset;
}

off_t command_buffer_t::write_offset() {
    return data_.write_offset;
}

void command_buffer_t::read_offset( off_t offset ) {
    data_.read_offset = offset;
}

void command_buffer_t::write_offset( off_t offset ) {
    data_.write_offset = offset;
}

void command_buffer_t::reset_read_offset() {
    read_offset( 0 );
}

void command_buffer_t::reset_write_offset() {
    write_offset( 0 );
}

void command_buffer_t::reset_offsets() {
    reset_read_offset();
    reset_write_offset();
}
