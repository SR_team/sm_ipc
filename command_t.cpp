#include "command_t.h"

#include "host_info_t.h"

command_t::deleter::deleter( host_info_t *host ) : host( host ) {
    host->command().status = command_t::status_t::wait;
}

command_t::deleter::~deleter() {
    host->command().status = command_t::status_t::terminate;
    host->command().id = invalid_command_id;
}
