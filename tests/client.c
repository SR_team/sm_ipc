#include <stdio.h>
#if __has_include( <windows.h>)
#	include <windows.h>
#else
#	include <unistd.h>
#endif

#include "c_api.h"

int do_process = 1;
int process_command( struct raw_command_buffer_t *buf ) {
    printf( "Hello C-world!\n" );
    do_process = 0;
    return 1;
}

void write_command_args( struct raw_command_buffer_t *buffer, enum action_cmd action ) {
    if ( action != action_cmd_write ) return;
#if __has_include( <windows.h>)
    *( (int *)buffer->data ) = GetCurrentProcessId();
#else
    *( (int *)buffer->data ) = getpid();
#endif
    buffer->write_offset = sizeof( int );
}

int main() {
    printf( "Connect C-client to %d\n", ipc_get_host_pid() );

    void *shm_ptr = ipc_alloc( "test_alloc", 8192, host_target_pid );
    printf( "Allocate memory at %p\n", shm_ptr );
    struct ipc_ptr_t ipc_ptr = ipc_find( "test_alloc", host_target_pid );
    printf( "Find memory at %p %ldB\n", ipc_ptr.ptr, ipc_ptr.size );
    printf( "Deallocate memory %d\n", ipc_free( "test_alloc", host_target_pid ) );

    ipc_register_cmd( 1, process_command );
    ipc_call_cmd( 1, write_command_args, host_target_pid, wait_default );

    while ( do_process ) ipc_process();

    printf( "Exit C-client\n" );

    return 0;
}
