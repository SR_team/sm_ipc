#include <iostream>
#include <chrono>
#include <thread>
#include <filesystem>

#include <boost/process/child.hpp>
#include <boost/config.hpp>

#ifdef BOOST_OS_WINDOWS

#	include <windows.h>

static bool kill( int id ) {
	auto handle = OpenProcess( PROCESS_TERMINATE, FALSE, id );
	if ( !handle )
		return false;

	FreeConsole();
	if ( !AttachConsole( id ) ) {
		CloseHandle( handle );
		return false;
	}

	// Disable Ctrl-C handling for our program
	SetConsoleCtrlHandler( NULL, true );

	GenerateConsoleCtrlEvent( CTRL_C_EVENT, 0 ); // SIGINT

	//Re-enable Ctrl-C handling or any subsequently started
	//programs will inherit the disabled state.
	SetConsoleCtrlHandler( NULL, false );

	CloseHandle( handle );
	return true;
}

static constexpr auto kServerExe = "sm_ipc_test_server.exe";
static constexpr auto kClientExe = "sm_ipc_test_client.exe";
static constexpr auto kClientCExe = "sm_ipc_test_client_c.exe";
#else
#	include <signal.h>
static bool kill( int id ) {
	return ::kill( id, SIGTERM ) != -1;
}

static constexpr auto kServerExe = "sm_ipc_test_server";
static constexpr auto kClientExe = "sm_ipc_test_client";
static constexpr auto kClientCExe = "sm_ipc_test_client_c";
#endif

using namespace std::chrono_literals;

boost::process::child run_test( const std::filesystem::path &executable ) {
	auto path = std::filesystem::current_path();
	std::filesystem::current_path( executable.parent_path() );
	boost::process::child child( executable.filename().string() );
	std::filesystem::current_path( path );
	return child;
}

bool process_child( boost::process::child &server, const std::filesystem::path &executable ) {
	auto client = run_test( executable );
	if ( !client.wait_for( 10s ) ) {
		std::cerr << "Too long testing client " << client.id() << std::endl;
		client.terminate();
		kill( server.id() );
		std::this_thread::sleep_for( 2s );
		if ( server.joinable() )
			server.terminate();
		return false;
	}

	if ( client.exit_code() ) {
		std::cerr << "Client " << client.id() << " return error " << client.exit_code() << std::endl;
		return false;
	}

	return true;
}

std::filesystem::path prepare_test( const std::filesystem::path &test ) {
	auto base_path = "tests" / test.filename() / "sm_ipc";
	if ( !std::filesystem::exists( base_path ) )
		std::filesystem::create_directories( base_path );
	auto path = base_path / ( std::string( "test" ) + ( test.has_extension() ? test.extension().string() : "" ) );
	if ( std::filesystem::exists( path ) )
		std::filesystem::remove( path );
	std::filesystem::copy_file( test, path );
	return path;
}

int main() {
	auto server_path = prepare_test( kServerExe );
	auto server = run_test( server_path );
	if ( !server.joinable() ) {
		std::cerr << "Server is not started or crashed" << std::endl;
		return 1;
	}

	std::this_thread::sleep_for( 1s );

	auto client_path = prepare_test( kClientExe );
	auto client_c_path = prepare_test( kClientCExe );

	for ( int i = 0; i < 100; ++i ) {
		if ( !process_child( server, client_path ) )
			return 1;
		if ( !process_child( server, client_c_path ) )
			return 1;
	}

	std::filesystem::remove( client_c_path );
	std::filesystem::remove( client_path );

	kill( server.id() );
	if ( server.joinable() )
		server.join();

	std::filesystem::remove( server_path );

	return 0;
}
