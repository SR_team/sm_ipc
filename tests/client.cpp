#include <iostream>

#include <boost/process/environment.hpp>

#include "Ipc.h"

using namespace std::chrono_literals;

static constexpr auto kMemoryName = "test_alloc";
static constexpr auto kAllocateMemory = 4096;

int main() {
	std::cout << "Connect client to " << Ipc::get().get_host_pid() << std::endl;
	Ipc::get().process();
	auto mem = Ipc::get().alloc( kMemoryName, kAllocateMemory );

	if ( mem != nullptr ) {
		std::cout << "Allocate memory at " << (void *)mem << std::endl;

		auto ptr = Ipc::get().find( kMemoryName );
		std::cout << "Find memory at " << (void *)ptr.ptr << " " << ptr.size << "B" << std::endl;

		if ( ptr.size != kAllocateMemory )
			std::cerr << "Allocated " << ptr.size << "B instead of required " << kAllocateMemory << "B" << std::endl;

		std::cout << "Deallocate memory " << std::boolalpha << Ipc::get().free( kMemoryName ) << std::endl;
	} else
		std::cerr << "Allocator return nullptr!" << std::endl;

	bool exit = false;
	Ipc::get().registr_cmd( 1, [&exit]( command_buffer_t & ) {
		std::cout << "Hello world!" << std::endl;
		exit = true;
		return true;
	} );

	Ipc::get().call_cmd(1, host_target_pid, 30s, boost::this_process::get_id());

	while ( !exit )
		Ipc::get().process();

	std::cout << "Exit client" << std::endl;
	return 0;
}
