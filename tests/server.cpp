#include <csignal>
#include <cstdio>
#include <iostream>

#include <boost/process/environment.hpp>
#include <boost/interprocess/exceptions.hpp>

#include "Ipc.h"

static bool g_work = true;

void exit_server( int reason ) {
    std::cout << "Exit server " << reason << std::endl;
    g_work = false;
}

int main()
{
    std::cout << "Start server " << Ipc::get().get_host_pid() << std::endl;
    if ( Ipc::get().get_host_pid() != boost::this_process::get_id() )
        std::cout << "Real server id " << boost::this_process::get_id() << std::endl;

    signal( SIGTERM, exit_server );
    signal( SIGINT, exit_server );

    auto client_pid = 0;
    Ipc::get().registr_cmd( 1, [&client_pid]( command_buffer_t &buffer ) {
        buffer.reset_read_offset();
        client_pid = buffer.read<int>();
        return true;
    } );

    while ( g_work ) {
        Ipc::get().process();
        if ( client_pid ) {
            Ipc::get().call_cmd( 1, client_pid );
            client_pid = 0;
        }
    }
    return 0;
}
